# Datanucleus JPA example #

According to: "The mechanism provided by DataNucleus is to use an "enhancer" process to perform this manipulation before you use your classes at runtime."
(optional) mvn: -clean:clean

Compile project (Ctrl+Shift+F9)
Check that META-INF fo;der was added to "target".
mvn: -datanucleus:enhance

Now, you can run "Main" or tests.


